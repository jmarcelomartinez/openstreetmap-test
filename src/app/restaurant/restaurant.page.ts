import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.page.html',
  styleUrls: ['./restaurant.page.scss'],
})
export class RestaurantPage implements OnInit {

  id: number = null
  name: string = null

  constructor(private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.id = parseInt(this.actRoute.snapshot.paramMap.get("id"));
    this.name = this.actRoute.snapshot.paramMap.get("name");

  }

}
