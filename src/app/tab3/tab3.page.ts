import { Component } from '@angular/core';
import { Map, tileLayer, marker, icon, Marker } from 'leaflet';
import { Router } from "@angular/router";
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  map: Map;
  restaurantsList = [];

  constructor(private router: Router) { }

  resetBrokeUrlIcons() {
    const iconRetinaUrl = 'assets/marker-icon-2x.png';
    const iconUrl = 'assets/marker-icon.png';
    const shadowUrl = 'assets/marker-shadow.png';
    const iconDefault = icon({
      iconRetinaUrl,
      iconUrl,
      shadowUrl,
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      tooltipAnchor: [16, -28],
      shadowSize: [41, 41]
    });
    Marker.prototype.options.icon = iconDefault;
  }

  getData() {
    fetch('./assets/data.json').then(res => res.json())
      .then(json => {
        this.restaurantsList = json.restaurants;
        console.log(this.restaurantsList);
        this.leafletMap();
      });
  }
  ionViewDidEnter() {

    this.resetBrokeUrlIcons();

    this.map = new Map('mapId3').setView([0.3468, -78.1323], 14);

    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    this.getData();
  }

  leafletMap() {
    for (let restaurant of this.restaurantsList) {
      let id = restaurant.id ; 
      let name = restaurant.name ;
      marker([parseFloat(restaurant.long), parseFloat(restaurant.lat)]).addTo(this.map)
        .bindPopup(restaurant.name, { autoClose: false , closeButton: false })
        .on('click', () => this.router.navigate(["restaurant",{id,name}]))
        .openPopup();
        console.log("id: "+id + " name: "+ name)
    }
  }

  ionViewWillLeave() {
    this.map.remove();
  }

}
